var express = require('express');
var router = express.Router();

/* GET home page. */

router.get('/', function (req, res) {
  var request = require('request');
  request({
    url: process.env.API_URI + '/count/',
    headers: {
      'Authorization': process.env.WEBSERVICE_API_KEY
    },
    json: true
  }, function callback(error, response, body) {
    if (error)
      return res.render('layout/error', {
        message: 'Error',
        error: error
      });

    if (response.statusCode == 200) {
      var os = require("os");
      if(!req.session.count) req.session.count = 0

      req.session.count = req.session.count + 1

      res.render('index', {
        count: req.session.count,
        uihostname: os.hostname(),
        subscribed: body.subscribed,
        apihostname: body.apihostname,
      });
    } else {
      console.log(body)
      res.render('layout/error', {
        message: 'Error',
        error: body
      });
    }
  });
});

router.post('/', function (req, res) {
  var request = require('request');
  request.post({
    url: process.env.API_URI + '/add/',
    form: {
      email: req.body.email
    },
    headers: {
      'Authorization': process.env.WEBSERVICE_API_KEY
    },
    json: true
  }, function callback(error, response, body) {
    if (error)
      return res.render('layout/error', {
        message: 'Error',
        error: error
      });

    if (response.statusCode == 200) {
      res.redirect('/');
    } else {
      console.log(body)
      return res.redirect('/');
      res.render('layout/error', {
        message: 'Error',
        error: body
      });
    }
  });
});

module.exports = router;
