var gulp = require('gulp');
var pug = require('gulp-pug');
var sass = require('gulp-sass');
var gulpif = require('gulp-if');
var clean = require('gulp-clean');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var nodemon = require('gulp-nodemon');
var htmlmin = require('gulp-htmlmin');
var cleanCSS = require('gulp-clean-css');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync').create();

var reload = browserSync.reload;

var is_dev = process.env.NODE_ENV === 'production' ? false : true;

gulp.task('styles', function () {
  gulp.src('public/style.css', {read: false}).pipe(clean({force: true}));

  return gulp.src('src/styles/main.scss')
    .pipe(gulpif(is_dev, sourcemaps.init()))
    .pipe(sass().on('error', sass.logError))
    .pipe(cleanCSS())
    .pipe(autoprefixer([
      'ie >= 10',
      'ie_mob >= 10',
      'ff >= 30',
      'chrome >= 34',
      'safari >= 7',
      'opera >= 23',
      'ios >= 7',
      'android >= 4.4',
      'bb >= 10'
    ]))
    .pipe(gulpif(is_dev, sourcemaps.write()))
    .pipe(concat('style.css'))
    .pipe(gulp.dest('public'))
    .pipe(reload({stream: true}));
});

gulp.task('scripts', function(){
  gulp.src('public/scripts/**/*', {read: false}).pipe(clean({force: true}));

  return gulp.src("src/scripts/**/*.js")
    .pipe(gulpif(is_dev, sourcemaps.init()))
    .pipe(uglify())
    .pipe(gulpif(is_dev, sourcemaps.write()))
    .pipe(gulp.dest('public'))
    .pipe(reload({stream: true}));
});
/*
gulp.task('views', function () {
  return gulp.src(['src/views/*.pug'])
    .pipe(pug({
      debug: false,
      compileDebug: false
    }))
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('dist/views'))
    .pipe(reload({stream: true}));
});
*/
gulp.task('watch', ['build'], function(){
  gulp.watch('src/styles/**/*.scss', ['styles']);
  gulp.watch('src/scripts/**/*.js', ['scripts']);
  gulp.watch('public/images/**/*', reload);
  //gulp.watch('src/views/**/*.pug', ['views']);
  gulp.watch('src/views/**/*.pug', reload);
});

gulp.task('run', ['browserSync'], function () {
  nodemon({
    script: './bin/www',
    env: { 'NODE_ENV': 'development', 'PORT': (process.env.PORT || '80') }
  }).on('error', function(err) {
    throw err;
  });
});

gulp.task('browserSync', ['watch'], function() {
  return browserSync.init({
    proxy: {
      target: 'localhost:' + (process.env.PORT || '8080')
    },
    port: (process.env.PORT_SYNC || 80),
    open: false,
    notify: false
  });
});

gulp.task('build', ['styles', 'scripts']);
gulp.task('default', ['build', 'run']);
