# Docker Course Source Code

[![Build status](https://gitlab.com/kiyanir/docker-course/badges/master/build.svg)](https://gitlab.com/kiyanir/docker-course/pipelines)

This repository contains the source code of the From Local Docker Development to Production Deployments Course

## Getting started

Download [Docker](https://www.docker.com/products/overview). If you are on Mac or Windows, [Docker Compose](https://docs.docker.com/compose) will be automatically installed. On Linux, make sure you have the latest version of [Compose](https://docs.docker.com/compose/install/).

One small thing you have to do before starting is to copy the example environment file and set your secrets:

    cp .env.example .env

Now you can run on development environment:

    ./run.sh serve

Or on production environment:

    ./run.sh start

## Is it any good?

Contact me on [Twitter](https://twitter.com/kiyanir) and tell me your feedback.
