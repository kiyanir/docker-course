<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Emails;

class IndexController extends Controller
{
    public function home()
    {
      return response()->json([
        'status_code' => 200
      ], 200);
    }

    public function count(Request $request)
    {
        if (env('WEBSERVICE_API_KEY') != $request->header('Authorization')) {
          return response()->json([
              'message'     => 'Authorization Error',
              'status_code' => 400,
          ], 400);
        }

        $subscribed = Emails::count();
        $apihostname = $_SERVER['HOSTNAME'];

        return response()->json([
            'subscribed' => $subscribed,
            'apihostname' => $apihostname,
            'status_code' => 200,
        ], 200);
    }

    public function add(Request $request)
    {
        if (env('WEBSERVICE_API_KEY') != $request->header('Authorization')) {
          return response()->json([
              'message'     => 'Authorization Error',
              'status_code' => 400,
          ], 400);
        }

        try {
            $this->validate($request, [
                'email' => 'required|email|max:255|unique:emails'
            ]);
        } catch (ValidationException $exception) {
            return response()->json([
                'message' => $exception->validator->getMessageBag(),
                'status_code' => 400,
            ], 400);
        }

        Emails::firstOrCreate([
            'email' => $request->input('email')
        ]);

        return response()->json([
          'status_code' => 200
        ], 200);
    }
}
