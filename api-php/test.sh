#!/bin/bash

# Wait unitl mysql up
while :
do
  (echo > /dev/tcp/dockercourse_mysql_test/3306) >/dev/null 2>&1
  if [[ $? -eq 0 ]]; then
    break
  fi
  sleep 1
done

php artisan migrate:refresh --seed --force

./vendor/phpunit/phpunit/phpunit
