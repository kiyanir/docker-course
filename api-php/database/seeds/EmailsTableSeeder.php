<?php

use Illuminate\Database\Seeder;

class EmailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Emails::class)->create([
            'email' => 'user1@example.com'
        ]);

        factory(App\Emails::class)->create([
            'email' => 'user2@example.com'
        ]);

        factory(App\Emails::class)->create([
            'email' => 'user3@example.com'
        ]);
    }
}
