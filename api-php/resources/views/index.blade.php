<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Laravel</title>
  <link href="/style.css" rel="stylesheet" type="text/css">
</head>
<body>

  <h1>Hello {{$name}}!</h1>
  <p>Hostname: {{$hostname}}</p>
  <p>WEBSERVICE_API_KEY: {{$WEBSERVICE_API_KEY}}</p>

</body>
</html>
