<?php

use Laravel\Lumen\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCount()
    {
        $this->json('get', '/count/', [], [])
            ->seeJson([
               'message' => 'Authorization Error',
               'status_code' => 400,
            ]);

        $this->json('get', '/count/', [], ['Authorization'=> env('WEBSERVICE_API_KEY')])
            ->seeJson([
               'subscribed' => 3,
               'status_code' => 200,
            ]);
    }
    public function testAdd()
    {
        $this->json('post', '/add/', [], [])
            ->seeJson([
               'message' => 'Authorization Error',
            ]);

        $this->json('post', '/add/', ['email' => 'email.com'], ['Authorization'=> env('WEBSERVICE_API_KEY')])
            ->seeJson([
               'email' => ['The email must be a valid email address.'],
            ]);

        $this->json('post', '/add/', ['email' => 'test@email.com'], ['Authorization'=> env('WEBSERVICE_API_KEY')])
            ->seeJson([
               'status_code' => 200,
            ]);

        $this->json('get', '/count/', [], ['Authorization'=> env('WEBSERVICE_API_KEY')])
            ->seeJson([
               'subscribed' => 4,
               'status_code' => 200,
            ]);
    }
}
