source .env

build() {
  docker-compose -f docker-compose-build.yml build $1
}

push() {
  docker-compose -f docker-compose-build.yml push
}

deploy() {
  docker login -u $GITLAB_REGISTRY_USER -p $GITLAB_REGISTRY_TOKEN registry.gitlab.com
  docker-compose pull
  docker-compose up -d
}

start() {
  docker-compose up
}

stop() {
  docker-compose stop
}

serve() {
  docker-compose -f docker-compose-development.yml up
}

test() {
  docker-compose -f docker-compose-test.yml up --abort-on-container-exit
  code=$(docker inspect -f '{{ .State.ExitCode }}' dockercourse_api_php_test)
  if [ "$code" != "0" ]; then
   exit -1
  fi
}

cache() {
  docker network create dockercourse_network
  docker-compose create api_php_1
  docker-compose create ui_1

  docker cp dockercourse_api_php_1:/var/www/html/vendor/. ./api-php/vendor/
  docker cp dockercourse_ui_1:/usr/src/node_modules/. ./ui/node_modules/
}

case "$1" in
  start)
    start
    ;;
  serve)
    serve
    ;;
  build)
    build $2
    ;;
  push)
    push
    ;;
  deploy)
    deploy
    ;;
  stop)
    stop
    ;;
  test)
    test
    ;;
  cache)
    cache
    ;;
  *)
    echo "Usage: $0 {start|stop|build|push|deploy|serve|test|cache}"
esac
