#!/bin/bash

set -e

if [ "$APP_ENV" == "production" ]; then
  sed -e '/#\[development\]/d' /usr/local/etc/haproxy/haproxy.cfg.tmp > /usr/local/etc/haproxy/haproxy.cfg
else
  sed -e '/#\[production\]/d' /usr/local/etc/haproxy/haproxy.cfg.tmp > /usr/local/etc/haproxy/haproxy.cfg
fi

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
	set -- haproxy "$@"
fi

if [ "$1" = 'haproxy' ]; then
	# if the user wants "haproxy", let's use "haproxy-systemd-wrapper" instead so we can have proper reloadability implemented by upstream
	shift # "haproxy"
	set -- "$(which haproxy-systemd-wrapper)" -p /run/haproxy.pid "$@"
fi

exec "$@"
