#!/bin/bash

if [ "$APP_ENV" == "production" ]; then
  sed -e '/#\[development\]/d' /etc/nginx/nginx.conf.tmp > /etc/nginx/nginx.conf
else
  sed -e '/#\[production\]/d' /etc/nginx/nginx.conf.tmp > /etc/nginx/nginx.conf
fi

exec "$@"
